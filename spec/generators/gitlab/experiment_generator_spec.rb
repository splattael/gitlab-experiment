# frozen_string_literal: true

require 'spec_helper'

# named class to test collisions
class TakenNameExperiment
end

RSpec.describe "Gitlab::Generators::ExperimentGenerator", :rails, type: :generator do
  tests { Gitlab::Generators::ExperimentGenerator }
  destination { Rails.root.join('generated') }
  let(:args) { %w[NullHypothesis foo bar baz] }

  it "generates the experiment correctly" do
    run_generator(args)

    expect(destination_root).to have_structure {
      file('app/experiments/null_hypothesis_experiment.rb') do
        contains <<~TEXT
          # frozen_string_literal: true

          class NullHypothesisExperiment < ApplicationExperiment
            # Describe your experiment:
            #
            # The variant behaviors defined here will be called whenever the experiment
            # is run unless overrides are provided.

            variant(:foo) { }
            variant(:bar) { }
            variant(:baz) { }
        TEXT
      end
    }
  end

  it "provides sensible defaults for variant names" do
    argument = described_class.arguments[1]
    expect(argument.name).to eq('variants')
    expect(argument.default).to eq(%w[control candidate])
  end

  context "when things aren't right" do
    it "handles class collisions nicely" do
      output = capture(:stderr) { run_generator(['TakenName']) }

      expect(output).to include <<~TEXT.squish.squeeze(' ')
        The name 'TakenNameExperiment' is either already used in your application or reserved by Ruby on Rails.
      TEXT
    end

    it "fixes things if you accidentally add _experiment to the name" do
      run_generator(['null_hypothesis_experiment'])

      expect(destination_root).to have_structure {
        file('app/experiments/null_hypothesis_experiment.rb')
      }
    end
  end
end
