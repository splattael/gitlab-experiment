# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "Gitlab::Generators::Experiment::InstallGenerator", :rails, type: :generator do
  tests { Gitlab::Generators::Experiment::InstallGenerator }
  destination { Rails.root.join('generated') }
  let(:args) { [] }
  let(:generated_output) { run_generator(args) }

  before do
    generated_output
  end

  it "installs the initializer and base class" do
    expect(destination_root).to have_structure {
      directory('config/initializers') do
        file('gitlab_experiment.rb') { contains 'Gitlab::Experiment.configure do |config|' }
      end
      directory('app/experiments') do
        file('application_experiment.rb') { contains 'class ApplicationExperiment < Gitlab::Experiment' }
      end
    }
  end

  it "displays a post install message" do
    expect(generated_output).to include <<~TEXT
      Gitlab::Experiment has been installed. You may want to adjust the configuration
      that's been provided in the Rails initializer.
    TEXT
  end

  context "when skipping the initializer" do
    let(:args) { ['--skip-initializer'] }

    it "doesn't install the initializer" do
      expect(destination_root).to have_structure {
        directory('config/initializers') { no_file 'gitlab_experiment.rb' }
      }
    end
  end

  context "when skipping the base class" do
    let(:args) { ['--skip-baseclass'] }

    it "doesn't install the base class" do
      expect(destination_root).to have_structure {
        directory('app/experiments') { no_file 'application_experiment.rb' }
      }
    end
  end
end
