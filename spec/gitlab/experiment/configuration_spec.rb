# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Configuration do
  it "responds to the expected getter/setter interface" do
    expect((described_class.methods - Object.methods).sort).to eq [
      :name_prefix, :name_prefix=,
      :logger, :logger=,
      :base_class, :base_class=,
      :strict_registration, :strict_registration=,
      :cache, :cache=,
      :default_rollout, :default_rollout=,
      :context_key_secret, :context_key_secret=,
      :context_key_bit_length, :context_key_bit_length=,
      :mount_at, :mount_at=,

      :redirect_url_validator, :redirect_url_validator=,
      :tracking_behavior, :tracking_behavior=,
      :nested_behavior, :nested_behavior=,
      :publishing_behavior, :publishing_behavior=,
      :cookie_domain, :cookie_domain=,

      # deprecated methods
      :context_hash_strategy=,
      :inclusion_resolver, :inclusion_resolver=,
      :variant_resolver, :variant_resolver=,

      :instance, :_load, # singleton assurances
      :deprecated # internal helper methods
    ].sort
  end

  describe ".default_rollout" do
    around do |example|
      original_rollout = config.default_rollout
      example.run
      config.default_rollout = original_rollout
    end

    describe "when specifying various arguments" do
      using RSpec::Parameterized::TableSyntax
      where(:rollout, :options, :expected_klass_and_options) do
        :random                             | nil            | [Gitlab::Experiment::Rollout::Random, {}]
        :percent                            | { foo: 'bar' } | [Gitlab::Experiment::Rollout::Percent, { foo: 'bar' }]
        'gitlab/experiment/rollout/random'  | nil            | [Gitlab::Experiment::Rollout::Random, {}]
        Gitlab::Experiment::Rollout::Random | { foo: 'bar' } | [Gitlab::Experiment::Rollout::Random, { foo: 'bar' }]
      end

      with_them do
        it "resolves the strategy" do
          config.default_rollout = rollout, options

          result = config.default_rollout
          expect(result).to be_a(Gitlab::Experiment::Rollout::Strategy)
          expect(result.klass).to be(expected_klass_and_options[0])
          expect(result.options).to eq(expected_klass_and_options[1])
        end
      end
    end

    context "when specifying a rollout strategy" do
      it "resolves to the correct strategy" do
        config.default_rollout = Gitlab::Experiment::Rollout::Strategy.new(
          Gitlab::Experiment::Rollout::Percent,
          foo: 'bar'
        )

        result = config.default_rollout
        expect(result).to be_a(Gitlab::Experiment::Rollout::Strategy)
        expect(result.klass).to be(Gitlab::Experiment::Rollout::Percent)
        expect(result.options).to eq(foo: 'bar')
      end
    end

    context "when specifying a rollout instance" do
      it "gives a deprecation warning" do
        expect(config).to receive(:deprecated)

        config.default_rollout = Gitlab::Experiment::Rollout::Percent.new(foo: 'bar')
      end

      it "resolves into being a strategy" do
        config.default_rollout = Gitlab::Experiment::Rollout::Percent.new(foo: 'bar')

        result = config.default_rollout
        expect(result).to be_a(Gitlab::Experiment::Rollout::Strategy)
        expect(result.klass).to be(Gitlab::Experiment::Rollout::Percent)
        expect(result.options).to eq(foo: 'bar', include_control: false)
      end
    end
  end

  context "with deprecations" do
    after do
      config.instance_variable_set(:@__inclusion_resolver, nil)
    end

    it "warns on #context_hash_strategy=" do
      expect(config).to receive(:deprecated).twice

      block = ->(*args) { args.join('|') }
      config.context_hash_strategy = block

      expect(config.instance_variable_get(:@__context_hash_strategy)).to eq(block)
      expect(experiment(:example).key_for('foo')).to eq('foo|gitlab_experiment_example')

      config.context_hash_strategy = nil
    end

    it "warns on variant_resolver getter/setter" do
      expect(config).to receive(:deprecated).exactly(3).times

      expect(config.variant_resolver).to eq(nil)

      config.variant_resolver = updated = -> {}

      expect(config.inclusion_resolver).to eq(updated)
    end

    it "warns on inclusion_resolver getter/setter" do
      expect(config).to receive(:deprecated).exactly(3).times

      expect(config.inclusion_resolver).to eq(nil)

      config.inclusion_resolver = block = -> { true }

      expect(config.inclusion_resolver).to eq(block)
    end
  end
end
