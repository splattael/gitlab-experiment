# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Rollout::Random do
  let(:subject_experiment) { Gitlab::Experiment.new(:example) }

  before do
    stub_experiments(example: true)
    subject_experiment.rollout(described_class)
  end

  it "returns whatever random result sample pulls out" do
    expect(subject_experiment.variant_names).to receive(:sample).and_return(:variant2)

    subject_experiment.variant(:variant1) {}
    subject_experiment.variant(:variant2) {}

    expect(subject_experiment.assigned.name).to eq('variant2')
  end
end
