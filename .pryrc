# frozen_string_literal: true

Pry.config.prompt_name = "\e[0;35mgitlab-experiment \e[0m"
