# Release Process

## Versioning

We follow [Semantic Versioning](https://semver.org).  In short, this means that the new version should reflect the types of changes that are about to be released.

*summary from semver.org*

MAJOR.MINOR.PATCH

- MAJOR version when you make incompatible API changes,
- MINOR version when you add functionality in a backwards compatible manner, and
- PATCH version when you make backwards compatible bug fixes.

## When we release

We release `gitlab-experiment` on an ad-hoc basis. There is no regularity to when we release, we simply release when the changes warrant a new release. This can be to fix a bug, or to implement new features and/or deprecate existing ones.

## How-to

To release, run `bin/release stage` and just follow the instructions!
