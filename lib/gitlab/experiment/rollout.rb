# frozen_string_literal: true

module Gitlab
  class Experiment
    module Rollout
      autoload :Percent, 'gitlab/experiment/rollout/percent.rb'
      autoload :Random, 'gitlab/experiment/rollout/random.rb'
      autoload :RoundRobin, 'gitlab/experiment/rollout/round_robin.rb'

      def self.resolve(klass, options = {})
        case klass
        when String
          Strategy.new(klass.classify.constantize, options)
        when Symbol
          Strategy.new("#{name}::#{klass.to_s.classify}".constantize, options)
        when Class
          Strategy.new(klass, options)
        else
          raise ArgumentError, "unable to resolve rollout from #{klass.inspect}"
        end
      end

      class Base
        DEFAULT_OPTIONS = {
          include_control: false
        }.freeze

        attr_reader :experiment, :options

        delegate :variant_names, :cache, :id, to: :experiment

        def initialize(options = {})
          @options = DEFAULT_OPTIONS.merge(options)
        end

        def for(experiment)
          raise ArgumentError, 'you must provide an experiment instance' unless experiment.class <= Gitlab::Experiment

          @experiment = experiment

          self
        end

        def enabled?
          require_experiment(__method__)

          true
        end

        def resolve
          require_experiment(__method__)

          return nil if @experiment.respond_to?(:experiment_group?) && !@experiment.experiment_group?

          validate! # allow the rollout strategy to validate itself

          assignment = execute_assignment
          assignment == :control ? nil : assignment # avoid caching control
        end

        protected

        def validate!
          # base is always valid
        end

        def execute_assignment
          behavior_names.first
        end

        private

        def require_experiment(method_name)
          return if @experiment.present?

          raise ArgumentError, "you need to call `for` with an experiment instance before chaining `#{method_name}`"
        end

        def behavior_names
          options[:include_control] ? [:control] + variant_names : variant_names
        end
      end

      Strategy = Struct.new(:klass, :options) do
        def for(experiment)
          klass.new(options).for(experiment)
        end
      end
    end
  end
end
