# frozen_string_literal: true

module Gitlab
  class Experiment
    VERSION = '0.7.0'
  end
end
